package com.arkstudio.discoview

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import android.text.InputType
import android.widget.FrameLayout


/**
 * @author ChongOS
 * @since 08-Jan-2019
 */
class MainActivity : AppCompatActivity() {

    companion object {

        private const val TAG = "MainActivity"
        private const val AUTO_HIDE_DELAY_MILLIS = 1000L * 3
        private const val AUTO_GO_HOME_DELAY_MILLIS = 1000L * 60 * 5
        private const val AUTO_RELOAD_DELAY_MILLIS = 1000L * 60 * 60

        private const val PASSWORD = "dwel9333"
    }

    private val mHideHandler = Handler()
    private val mHideRunnable = Runnable { hideSystemUI() }
    private val mReloadHandler = Handler()
    private val mReloadRunnable = Runnable { web_view.loadUrl(web_view.url) }
    private val mGoHomeHandler = Handler()
    private val mGoHomeRunnable = Runnable { web_view.loadUrl(getHomeURL()) }
    private var mFullscreen = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        mFullscreen = isFullscreenEnabled()

        initialWebView()
    }

    override fun onStart() {
        super.onStart()

        window.decorView.setOnSystemUiVisibilityChangeListener {
            if (it == 0) delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }

        web_view.loadUrl(getHomeURL())
    }

    override fun onResume() {
        super.onResume()

        val fullscreen = isFullscreenEnabled()
        if(mFullscreen != fullscreen) {
            recreate()
        }

        if (mFullscreen) {
            hideSystemUI()
        } else {
            showSystemUI()
        }
    }

    override fun onStop() {
        mReloadHandler.removeCallbacks(mReloadRunnable)
        mGoHomeHandler.removeCallbacks(mGoHomeRunnable)
        window.decorView.setOnSystemUiVisibilityChangeListener(null)

        super.onStop()
    }

    override fun onDestroy() {
        mHideHandler.removeCallbacks(mHideRunnable)

        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_settings -> {
                showInputPasswordDialog{
                    if(PASSWORD == it)
                        startActivity(Intent(this, SettingsActivity::class.java))
                    else
                        AlertDialog.Builder(this)
                            .setMessage(R.string.err_incorrect_password)
                            .setPositiveButton(R.string.button_ok) { dialog, _ ->
                                dialog.cancel()
                            }
                            .show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun getHomeURL() = PreferenceManager.getDefaultSharedPreferences(this).getString(
        getString(R.string.pref_key_home_url),
        getString(R.string.pref_default_home_url)
    )?.let {
        var url = it
        if (!url.startsWith("www.") && !url.startsWith("http://") && !url.startsWith("https://"))
            url = "www.$url"

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://$url"

        url
    }

    private fun isFullscreenEnabled() = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
        getString(R.string.pref_key_fullscreen),
        true
    )

    @SuppressLint("SetJavaScriptEnabled")
    private fun initialWebView() {
        web_view.webViewClient = MyWebViewClient().apply {
            onPageFinishedListener = object : MyWebViewClient.OnPageFinishedListener {
                override fun onPageFinished(url: String?) {
                    Log.d(TAG, "onPageFinish: $url")
                    scheduleReload()
                }
            }
        }
        web_view.setOnTouchListener { _, _ ->
            scheduleGoHome()
            scheduleReload()
            false
        }
        web_view.settings.javaScriptEnabled = true
        web_view.settings.mediaPlaybackRequiresUserGesture = false
    }

    private fun scheduleGoHome() {
        Log.d(TAG, "scheduled to back home by $AUTO_GO_HOME_DELAY_MILLIS milliseconds.")
        mGoHomeHandler.removeCallbacks(mGoHomeRunnable)
        mGoHomeHandler.postDelayed(mGoHomeRunnable, AUTO_GO_HOME_DELAY_MILLIS)
    }

    private fun scheduleReload() {
        Log.d(TAG, "scheduled to reload by $AUTO_RELOAD_DELAY_MILLIS milliseconds.")
        mReloadHandler.removeCallbacks(mReloadRunnable)
        mReloadHandler.postDelayed(mReloadRunnable, AUTO_RELOAD_DELAY_MILLIS)
    }

    private fun Activity.showInputPasswordDialog(onSubmit: (String) -> Unit) {
        val layout = FrameLayout(this)
        layout.setPadding(
            resources.getDimensionPixelOffset(R.dimen.dialog_padding_start),
            resources.getDimensionPixelOffset(R.dimen.dialog_padding_top),
            resources.getDimensionPixelOffset(R.dimen.dialog_padding_end),
            0
        )
        val input = EditText(this)
        input.setSingleLine()
        input.setHint(R.string.password)
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        layout.addView(input)

        AlertDialog.Builder(this)
            .setTitle(R.string.input_password)
            .setView(layout)
            .setPositiveButton(R.string.button_ok) { dialog, _ ->
                onSubmit(input.text.toString())
                dialog.cancel()
            }
            .setNegativeButton(R.string.button_cancel) { dialog, _ ->
                dialog.cancel()
            }
            .show()
    }

    private fun showSystemUI() {
        if (!mFullscreen) {
            container.fitsSystemWindows = true
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        }

    }

    private fun hideSystemUI() {
        if (mFullscreen) {
            container.fitsSystemWindows = false
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)
        }
    }

    private fun delayedHide(delayMillis: Long) {
        if (mFullscreen) {
            mHideHandler.removeCallbacks(mHideRunnable)
            mHideHandler.postDelayed(mHideRunnable, delayMillis)
        }
    }

}

