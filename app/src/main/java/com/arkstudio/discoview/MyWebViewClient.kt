package com.arkstudio.discoview

import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

/**
 * @author ChongOS
 * @since 08-Jan-2019
 */
class MyWebViewClient : WebViewClient() {

    var alwayOverrideUrlLoading = false
    var onPageFinishedListener: OnPageFinishedListener? = null

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        return alwayOverrideUrlLoading && super.shouldOverrideUrlLoading(view, request)
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        onPageFinishedListener?.onPageFinished(url)
    }

    interface OnPageFinishedListener {
        fun onPageFinished(url: String?)
    }
}